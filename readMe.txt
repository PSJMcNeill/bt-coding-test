BT Coding Test
Program was created using Java (JDK 1.8).
How to run?
Program can be run from a command terminal using the following commands:

-javac dependencies.java

-java dependencies packages.txt gui swingui

Where packages.txt is equal to the text file to be used and any text after that are the remaining arguments for the program