import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.io.*;
import java.util.LinkedHashSet;
import java.util.Map;


public class dependencies {
	
	/**Main puts text file argument into a string, takes remaining arguments and stores in an ArrayList **/
	public static void main(String[] args)throws IOException{
		ArrayList<String> input = new ArrayList<String>();
		String packageFile= args[0];
		
				for(int i=1;i<=args.length-1;i++){
					input.add(args[i]);
				}
				
		//Call to function to read data contained in the text file, ArrayList and String are passed 		
		readFile(input,packageFile);		
	}
	
	
	/**readfile takes parameters that contain ArrayList with arguments from command line as well as the text file **/
	public static void readFile(ArrayList<String> input, String packageFile) throws IOException
	{
		//HashMap is used to store data from text file
		Map<String,ArrayList<String>> map = new HashMap<String,ArrayList<String>>();
		
		//Scanner used to read through text file
		Scanner file = new Scanner(new File(packageFile));
		
		try{
			//While loop will read each line and split via white space and store in an ArrayList
			while(file.hasNextLine())
			{
				String[] split = file.nextLine().split(" ");
				/*If second index of ArrayList does not contain -> system will terminate
				* This also means the first index must contain a valid package name 
				*/
				if(!(split[1].equals("->")))
				{
				System.out.println("Data is incorrect please amend and restart program");
				System.exit(0);
				}else
				{
				//If checks are valid will add each value to an ArrayList
					ArrayList<String> values = new ArrayList<String>();
						for(int P=2;P<=split.length-1;P++)
						{
							values.add(split[P]);
						}
				//Packages name is added as key to HashMap, ArrayList of values is added to values of HashMap
				map.put(split[0], values);
				}
			}
		}finally{
			file.close();
		}
		/*HashMap is now complete and calls next function
		 * passing the values of the HashMap and the input ArrayList of arguments
		 */
		findDepend(map, input);
	}
	
	/**This function will compare arguments with HashMap and return the corresponding packages**/
	public static void findDepend(Map<String,ArrayList<String>> map, ArrayList<String> input)
	{
			
		for(int i=0; i < input.size(); i++)
			{
				ArrayList<String> packages = new ArrayList<String>();
				ArrayList<String> all = new ArrayList<String>();
				all = new ArrayList<String>(new LinkedHashSet<String>(all));
				
				//If statement means if package has no dependencies it will just return the package
				if(!(map.containsKey(input.get(i))))
				{
					
				}else{
				//ArrayList adds all values from HashMap that match the key and the argument from the command line
				packages.addAll(map.get(input.get(i)));
				all.addAll(map.get(input.get(i)));
				//For Loop checks for transitive dependencies and adds them to the ArrayList
					for(int j=0; j < packages.size(); j++)
						{
							if(map.containsKey(packages.get(j)))
							{
							   all.addAll(map.get(packages.get(j)));
							}
						}
					}
				
				//Linked HashSet is used to remove duplicates	
				Set<String> set = new HashSet<>();
				set.addAll(all);
				all.clear();
				all.addAll(set);
				/*If statement checks for circular dependencies and removes any that are found*/
				if(all.contains(input.get(i)))
				{
					all.remove(input.get(i));
				}
				//Collections used to sort alphabetically
				Collections.sort(all);
				
				/*StringBuilder is used to give better formatting in final display*/
				StringBuilder format = new StringBuilder();
				for(String value : all)
				{
					format.append(value + " ");
				}
				String finalText = format.toString();
				
				//Prints out the dependencies required
				System.out.println(input.get(i) + " ->" + finalText);
			}
		
			
	}
}

